import HessianJS from 'hessian.js';
import * as HttpClientUtils from 'http-client-utils';

export default class HessianClientUtils {
    constructor(url) {
        this._url = url;
    }

    async invoke(method, ...argv) {
        let body = new Buffer([0x00, 0x01, 0x00, 0x00]);
        body.write('H');
        body.writeUInt16BE(0x0200, 1);
        body.write('C', 3);

        const encoder = new HessianJS.EncoderV2();

        encoder.writeString(method);
        encoder.writeInt(argv.length);

        for (const argument of argv) {
            encoder.write(argument);
        }

        body = Buffer.concat([body, encoder.get()]);

        const result = await HttpClientUtils.postBinary(this._url, body,
            true);

        if (result.statusCode !== 200) {
            throw new Error('http request error');
        } else {
            return new HessianJS.DecoderV2(result.body
                .slice(4)).read();
        }
    }
}
